import { Component, OnInit } from '@angular/core';
import * as CanvasJS from './canvasjs-2.3.2/canvasjs.min';
import { delay } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'liss';

  A: number;
  B: number;
  angle: number;
  a: number;
  b: number;

  ngOnInit(){
    this.A = 10;
    this.a = 1;
    this.angle = 0;
    this.B = 10;
    this.b = 14;
    this.draw();
  }

  submitData(A: number, B: number, angle: number, a: number, b: number){
    console.log("Data is a s follows: "
    + A
    + ", "
    + B
    + ", "
    + angle
    + ", "
    + a
    + ", "
    + b)
  };

  createChart(A: number, a: number, angle: number, B: number, b: number) {
    var dataPoints = [];
    var xVal;
    var yVal;
    var dataLength = 361;
    let chart = new CanvasJS.Chart("chartContainer", {
      theme: "dark1",
      title: {
        text: "L I S S A J O U S"
      },
      subtitles:[
        {text:"x = Asin( at + δ )"},
        {text:"y = Bsin( bt )"},
      ],
      axisX:{
        minimum:-20,
        maximum:20,
      },
      axisY:{
        minimum:-20,
        maximum:20,
      },
      data: [
        {
          type: "line",
          lineColor:"#69F0AE",
          dataPoints: dataPoints
        }
      ]
    });
    addDataPoints();

    chart.render();

    function addDataPoints() {
      var converter = Math.PI/180;
      for (var i = 0; i < dataLength; i++) {
        xVal = A * Math.sin(a * i * converter + angle * converter);
        yVal = B * Math.sin(b * i * converter)
        dataPoints.push({
          x: xVal,
          y: yVal
        });
      }
    }
  }

  draw() {
    this.createChart(this.A, this.a, this.angle, this.B, this.b);
  }

  increaseA() {
    if (this.A < 20) {
      this.A += 1;
      this.draw();
    }
  }

  decreaseA() {
    if (this.A > 0) {
      this.A -= 1;
      this.draw();
    }
  }

  increaseB() {
    if (this.B < 20) {
      this.B += 1;
      this.draw();
    }
  }

  decreaseB() {
    if (this.B > 0) {
      this.B -= 1;
      this.draw();
    }
  }

  increasea() {
    if (this.a < 20) {
      this.a += 1;
      this.draw();
    }
  }

  decreasea() {
    if (this.a > 0) {
      this.a -= 1;
      this.draw();
    }
  }

  increaseb() {
    if (this.b < 20) {
      this.b += 1;
      this.draw();
    }
  }

  decreaseb() {
    if (this.b > 0) {
      this.b -= 1;
      this.draw();
    }
  }

  increaseAngle() {
    if (this.angle < 90) {
      this.angle += 0.125;
      this.draw();
    }
  }

  decreaseAngle() {
    if (this.angle > 0) {
      this.angle -= 0.125;
      this.draw();
    }
  }





}
